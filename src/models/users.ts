import mongoose, { Schema, model, Document } from 'mongoose';

export interface UserDocument extends Document {
  userEmail: string;
  password: string;
  OTP: number;
  userStatus: string;
}

interface User {
  userEmail: string;
  password: string;
  userStatus: string; // Update the type to string
  OTP: number;
}

const userSchema = new Schema<User>({
  userEmail: { type: String, required: true },
  password: { type: String, required: true },
  userStatus: { type: String, default: "pending" },
  OTP: { type: Number }
});

const UserModel = model<UserDocument>('User', userSchema);
export default UserModel;
