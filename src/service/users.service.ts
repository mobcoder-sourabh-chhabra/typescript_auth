import { Document, FilterQuery, UpdateQuery, QueryOptions } from 'mongoose';
type DocumentDefinition<T> = Omit<T, keyof Document> & Partial<Document>;


import UserModel,{UserDocument} from '../models/users';


export function createUser(input:DocumentDefinition<UserDocument>){
    return UserModel.create(input)
}



export function findUser(query:FilterQuery<UserDocument>,options:QueryOptions={lean:true}){
    return UserModel.find(query,{},options);
}


export function findAndUpdate(
    query:FilterQuery<UserDocument>,
    update:UpdateQuery<UserDocument>,
    options:QueryOptions
){
    return UserModel.findOneAndUpdate(query,update,options)
}



export function deleteUser(query:FilterQuery<UserDocument>){
    return UserModel.deleteOne(query);
}