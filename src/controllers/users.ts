import { Request, Response } from 'express';
import UserModel, { UserDocument } from '../models/users';
import bcrypt from 'bcryptjs';
import sgMail from '@sendgrid/mail';

sgMail.setApiKey('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'); // Replace with your actual SendGrid API key

const userRegister = async (req: Request, res: Response) => {
  let userEmail = req.body.userEmail;
  const user: UserDocument[] = await UserModel.find({ userEmail: userEmail });


  if (user.length === 0) {
    try {
      const hash = await bcrypt.hash(req.body.password, 10);

      const otp = Math.floor(1000 + Math.random() * 9000);
      const msg = {
        to: req.body.userEmail,
        from: 'sourabh.chhabra@mobcoder.com',
        subject: 'OTP for your account',
        text: `Your OTP is: ${otp}`,
      };

      await sgMail.send(msg);

      const newUser = new UserModel({
        userEmail: req.body.userEmail,
        OTP: otp,
        msg: 'OTP SENT SUCCESSFULLY',
        password: hash,
      });

      const result = await newUser.save();

      res.status(200).json({
        Data: result,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    }
  } 
  
  else if (user[0].userStatus === 'active') {
    res.status(400).json({
      msg: 'Account is verified. Please login.',
    });
  } 
  
  else if(user[0].userStatus == 'pending') {
    try {
        const hash = await bcrypt.hash(req.body.password, 10);
  
        const otp = Math.floor(1000 + Math.random() * 9000);
        const msg = {
          to: req.body.userEmail,
          from: 'sourabh.chhabra@mobcoder.com',
          subject: 'OTP for your account',
          text: `Your OTP is: ${otp}`,
        };
  
        await sgMail.send(msg);
  
        const newUser = new UserModel({
          userEmail: req.body.userEmail,
          OTP: otp,
          
          password: hash,
        });
  
        const result = await newUser.save();
  
        res.status(200).json({
            msg: 'Please Verify your account, otp is send to mail',
          
        });
      } catch (err) {
        console.log(err);
        res.status(500).json({
          error: err,
        });
      }
  }
  
};

 const verifyOTP = async (req: Request, res: Response) => {
    try {
      const user = await UserModel.findOne({ userEmail: req.body.userEmail });
      console.log("user",user);
      
      if (!user) {
        res.status(400).json({
          msg: 'User not found.',
        });
        return;
      }
      
      if (req.body.OTP === user.OTP) {
        await UserModel.findOneAndUpdate(
          { userEmail: req.body.userEmail },
          { userStatus: 'active' }
        );
        
        res.status(200).json({
          msg: 'OTP VERIFIED SUCCESSFULLY',
        });
      } else {
        res.status(500).json({
          msg: 'OTP IS WRONG',
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    }
  };

    

   const forgotPassword = async (req: Request, res: Response) => {
    try {
      const userEmail = req.body.userEmail;
      const user: UserDocument[] = await UserModel.find({ userEmail: userEmail });
  
      if (user.length === 0) {
        res.status(400).json({
          msg: 'User not found.',
        });
        return;
      }
      if(user[0].userStatus == 'pending'){
        res.status(400).json({
            msg: 'Please Verify your account, otp is send to mail',
          });
          return;
      }
  
      const otp = Math.floor(1000 + Math.random() * 9000);
      const msg = {
        to: req.body.userEmail,
        from: 'sourabh.chhabra@mobcoder.com',
        subject: 'OTP for password reset',
        text: `Your OTP is: ${otp}`,
      };
  
      await sgMail.send(msg);
  
      await UserModel.findOneAndUpdate(
        { userEmail: req.body.userEmail },
        { OTP: otp }
      );
  
      res.status(200).json({
        msg: 'OTP has been sent to your email for password reset.',
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    }
  };
  
   const resetPassword = async (req: Request, res: Response) => {
    try {
      const userEmail = req.body.userEmail;
      const user: UserDocument[] = await UserModel.find({ userEmail: userEmail });
  
      if (user.length === 0) {
        res.status(400).json({
          msg: 'User not found.',
        });
        return;
      }
  
      if (req.body.OTP !== user[0].OTP) {
        res.status(400).json({
          msg: 'Invalid OTP.',
        });
        return;
      }
  
      const hash = await bcrypt.hash(req.body.password, 10);
      await UserModel.findOneAndUpdate(
        { userEmail: req.body.userEmail },
        { password: hash,userStatus:'active'  }
      );
  
      res.status(200).json({
        msg: 'Password has been reset successfully.',
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    }
  };



//   const bcrypt = require("bcrypt");
// const jwt = require("jsonwebtoken");
// const Userss = require("../models/userModel");

 

// const sgMail = require('@sendgrid/mail')
// const sgMailApiKey = ''
// sgMail.setApiKey(sgMailApiKey)




// exports.userRegisters = async (req, res) => {
//   userEmail= req.body.userEmail;
//   const user = await Userss.find({ userEmail: userEmail })

// if(user.length==0) {

//   const otp = Math.floor(1000 + Math.random() * 9000); // Generate a random 4 digit OTP
//   const msg = {
//     to: req.body.userEmail,
//     from: 'sourabh.chhabra@mobcoder.com',
//     subject: 'OTP for your account',
//     text: `Your OTP is: ${otp}`,
//   };
//   sgMail.send(msg) // Use the SendGrid API to send the OTP
//   .then((result) => {
//     const newUser = new Userss({
//       userEmail: req.body.userEmail,
//       OTP: otp,
//       msg: "OTP SENT SUCCESSFULLY"
//     });
//     newUser.save()
//     .then((result) => {
//       res.status(200).json({
      
//         msg: "Save Successfully"
//       });
//     })
//     .catch((err) => {
//       console.log(err);
//       res.status(500).json({
//         error: err,
//       });
//     });
//   })
//   .catch((err) => {
//     console.log(err);
//     res.status(500).json({
//       error: err,
//     });
//   });
// }
// else{
//   if(user[0].userVerifies=='false'){
//     const otp = Math.floor(1000 + Math.random() * 9000); // Generate a random 4 digit OTP
//   const msg = {
//     to: req.body.userEmail,
//     from: 'sourabh.chhabra@mobcoder.com',
//     subject: 'OTP for your account',
//     text: `Your OTP is: ${otp}`,
//   };
//   sgMail.send(msg) 
//     // res.status(200).json({
//     //   msg: "Your Email Not Verified OTP is send To Mail Please Verifies IT"
//     // })
//     .then((result) => {
//       Userss.findOneAndUpdate({userEmail: req.body.userEmail}, {OTP: otp}) // Use the Mongoose findOneAndUpdate() method to save the OTP to the user model
//       .then((result) => {
//         res.status(200).json({
//           msg: "Your Email Not Verified hence,OTP is send To Mail Please Verifies IT"
//         });
//       })
//       .catch((err) => {
//         console.log(err);
//         res.status(500).json({
//           error: err,
//         });
//       });
//     })
//     .catch((err) => {
//       console.log(err);
//       res.status(500).json({
//         error: err,
//       });
//     });
//   }
//   else{
//     res.status(200).json({
//       msg: "User Is Registered Successfully, Please Enter Another Mail To Continue"
//     });
//   }
// }

// };



// exports.getUserProfile = (req, res, next) => {
//   const userId = req.userData.userId;

//   Userss.findById({ _id: userId })
//     .exec()
//     .then((result) => {
//       res.status(200).json({
//         Data: result,
//       });
//     })
//     .catch((err) => {
//       console.log(err);
//       res.status(500).json({
//         error: err,
//       });
//     });
// };


// exports.updateEmail = (req, res) => {
//   const userId = req.userData.userId;

//   Userss.findOneAndUpdate(
//     { _id: userId },
//     {
//       $set: {
//         userEmail: req.body.userEmail,
//       },
//     }
//   )
//     .then((result) => {
//       res.status(200).json({
//         Data: result,
//       });
//     })
//     .catch((err) => {
//       res.status(500).json({
//         error: err,
//       });
//     });
// };





// exports.verifyOTP=async(req,res)=>{
//   let user = await Userss.findOne({userEmail:req.body.userEmail})
//   if(req.body.OTP == user.OTP){
//     Userss.findOneAndUpdate({userEmail:req.body.userEmail},{userVerifies:"true"})
//     .exec()
//     .then((result)=>{
//       res.status(200).json({
//         msg:"OTP VERIFIED SUCCESSFULLY"
//       });
//     })
//     .catch((err)=>{
//       console.log(err);
//       res.status(500).json({
//         error:err
//       });
//     });
//   }
//   else{
//     res.status(500).json({
//       msg:"OTP IS WRONG"
//     });
//   }
// }


// const addUser = async (req:Request,res:Response)=>{

// console.log("req.body.name",req.body.name);

// const user = await createUser ({name: req.body.name,dept:req.body.dept})

 
//     res.json({
//         message:"User added",
     
//         myData:user
//     })
// }




// const updateUser = async (req:Request,res:Response)=>{


//     const user = await findAndUpdate ({_id:req.params.id},{name:req.body.name,dept:req.body.dept},{new:true});
    
     
//         res.json({
//             message:"User Updated",
         
//             myData:user
//         })
//     }

//     const fetchUser = async (req:Request,res:Response)=>{


//         const user = await findUser({_id:req.params.id})
        
         
//             res.json({
               
//                 message:"User Information",
//                 myData:user
//             })
//         }

//         const deleteUsers = async (req:Request,res:Response)=>{


//             const user = await deleteUser({_id:req.params.id})
            
             
//                 res.json({
//                     message:"User Deleted",
                 
//                     myData:user
//                 })
//             }
export{
//     addUser,
//     updateUser,
//     fetchUser,
//     deleteUsers,
userRegister,
verifyOTP,
forgotPassword,
resetPassword
}