import express,{Request,Response} from 'express';

import { userRegister,verifyOTP,forgotPassword,resetPassword } from '../controllers/users';

 const router = express.Router();

 router.post("/addUser",userRegister)
 router.post("/verifyOTP",verifyOTP)
 router.post("/forgotPassword",forgotPassword)
 router.post("/resetPassword",resetPassword)



 

//  router.put("/updateUser/:id",updateUser)
//  router.get("/findUser/:id",fetchUser)
//  router.delete("/deleteUsers/:id",deleteUsers)




 router.get("/about",(req:Request,res:Response)=>{
    res.json({
        msg:"About Page"
    })
 })
 export {
    router
 }